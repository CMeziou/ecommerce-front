## Projet E-commerce Laure Chems Yann Samantha

Organisation: 

- Création d'issue dans le board,
- Création d'un git avec un sytème de branches, afin de pouvoir travailler et aider plus facilement,
- Des réunions, mise au point à chaque début de dession,
- Livecoding et explication afin de mettre au courant/à niveau les autres membres de l'équipe,
- Entre-aide sur nos branches et recherche,
- Création des pages et des components appelés dans celles-ci,
- Création des entités utilisées,
- Mise en place des paramètres de l'authentification et de son contexte,
- Parametrage du app.tsx pour ajouter le contexte, la navbar et le footer autour des différentes pages,
- Mise en place des différentes fonctions à appeler avec leurs services,
- Création d'un style css global

##Documents

Nos [Users stories, à voir ici.](https://docs.google.com/document/d/1nX7HICHSvX4IIngABOUL1_abs-Os2Z_kvlV_Fn_PzX0/edit?usp=sharing)

User Case: ![<alt>](</public/img/UseCaseDiagram1.jpg>)

Diagramme de classe: ![<alt>](</public/img/Diagramme_de_classe.png>)

Les Wireframes ont été réalisés avec [Figma, à voir ici.](https://www.figma.com/file/DUZ6Fjeh8Tq0O5HJuaa5hd/Ecommerce-Laure%2FChems%2FYann%2Fsam?node-id=0%3A1&t=mQGEtih8stPN8xpf-1)

## Les outils utilisés

- StarUML pour les diagrammes de classes et les userCases, 
- Figma pour les wireframes, 
- MariaDB pour la base de donnée MySQL, 
- Symfony pour le back avec Doctrine,
- React pour le front,
- Gitlab.

## Améliorations envisagées

Gestion compte utilisateur: 
- Rajouter un bouton "supprimer" pour les adresses
- Faire le responsive sur certaines pages
- Section commande : Ajouter les commandes visibles et créer un trie (par année, mois ?)
- Possibilité de demander à supprimer le compte

Ajout d'un produit avec upload d'image:
- terminer la branche avec le formulaire d'ajout d'un produit
- sécuriser la route pour les administrateurs seulement
- ajouter un bouton sur l'espace personnel d'un administrateur pour qu'il puisse ajouter un produit depuis son compte

Ajout des favoris:
- possibilité pour un user de marquer un produit en favoris et de le retrouver dans une liste sur son compte,
- marqueur visuel et ajout d'un compteur à côté des produits,
- utilisation de ce compteur pour mettre à jour la section des "articles favoris" sur l'accueil du site avec les articles les plus appréciés.

Panier:
- incrémentation d'une lineproduct lorsqu'elle est déjà présente dans le panier
- Ajout des fonctions liées à la validation du panier:
    - Mise à jour des stocks
    - Changement de statut d'une commande validée
    - Création d'une nouvelle commande en statut "panier".

Créer la barre de recherche et la relier au bouton de la navbar.

Terminer les réglages Css et responsive du site.

Ajout de tests pour les élément du front.



