import axios from "axios";
import { Product, Comment } from "./entities";

export async function fetchAllProduct() {
  const response = await axios.get<Product[]>("/api/product");
  return response.data;
}

export async function fetchOneProduct(id: number | string) {
  const response = await axios.get<Product>("/api/product/" + id);
  return response.data;
}

export async function postProduct(product: Product) {
  const response = await axios.post<Product>("/api/product", product);
  return response.data;
}

export async function fetchProductComments(id:number|string) {
    const response = await axios.get<Comment[]>(`/api/product/${id}/comments`);
    return response.data;
}