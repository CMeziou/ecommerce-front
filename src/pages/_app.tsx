import { AuthContextProvider } from '@/auth/auth-context';
import Footer from '@/components/Footer';
import '@/styles/globals.css';;
import axios from 'axios'
import type { AppProps } from 'next/app';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Navbar from '@/components/Navbar';;
import { useEffect } from 'react';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import '../auth/axios-config';
import { Category } from '@/entities';


axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;


export default function App({ Component, pageProps }: AppProps) {


  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);

  return (
    <AuthContextProvider>
      <Navbar/>
      <Component {...pageProps} />
      <Footer />
    </AuthContextProvider>
  )


}
