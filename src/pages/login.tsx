import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { User } from "@/entities";
import { Button, Card, Form, Input } from "antd";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { red } from "@ant-design/colors";
import Link from "next/link";

export default function loginPage() {
  const router = useRouter();
  const { setToken } = useContext(AuthContext);
  const [error, setError] = useState("");

  async function handleSubmit(values: User) {
    setError("");
    try {
      setToken(await login(values.mail, values.password));
      router.push("/account/");
    } catch (error: any) {
      if (error.response?.status == 401) {
        setError("Invalid login/password");
      } else {
        // setError('Erreur serveur');
        console.log(error);
      }
    }
  }

  return (
    <>
      <Card style={{ width: "75%", margin: "auto", marginTop: "20px" }}>
        <h1>Login</h1>
        {error && <p style={{ color: red.primary }}>{error}</p>}
        <Form onFinish={handleSubmit}>
          <Form.Item
            label="Email"
            name="mail"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Se connecter
            </Button>
          </Form.Item>

          <Link href={"/register"} className="btn">
            <div className="card-body">
              <h5 className="card-title">S'inscrire</h5>
            </div>
          </Link>

          <a className="item" onClick={() => setToken(null)}>
            Se déconnecter
          </a>
        </Form>
      </Card>
    </>
  );
}
