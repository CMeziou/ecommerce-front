import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { register } from "@/register-service";
import { User } from "@/entities";
import { Button, Card, Form, Input } from "antd";
import { useRouter } from "next/router";
import { useContext, useState } from "react";

export default function Register() {
  const router = useRouter();
  const { setToken } = useContext(AuthContext);
  const [error, setError] = useState("");
  const [users, setUsers] = useState();

    async function handleSubmit(values: User) {
        setError('');
        try {
            await register(values);

      setToken(await login(values.mail, values.password));
      router.push("/account/");
    } catch (error: any) {
      if (error.response?.status == 401) {
        setError("Invalid login/password");
      } else {
        // setError('Erreur serveur');
        console.log(error);
      }
    }
  }

  return (
    <>
      <Card style={{ width: "75%", margin: "auto", marginTop: "20px" }}>
        <h1>Créer un compte</h1>
        <Form onFinish={handleSubmit}>
          <Form.Item
            label="Prénom"
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item
            label="Nom"
            name="lastname"
            rules={[{ required: true, message: "Please input your lastName!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item
            label="Phone"
            name="phone"
            rules={[{ required: true, message: "Please input your phone!" }]}
          >
            <Input type="phone" />
          </Form.Item>

          <Form.Item
            label="Mail"
            name="mail"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Mot de passe"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Créer
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
}
