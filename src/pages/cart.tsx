import ItemCart from "@/components/ItemCart";
import { useEffect, useState } from "react";
import { Order } from "../../entities";
import { fetchCartProduct } from "../order-service";



export default function Cart() {

    const [panier, setPanier] = useState<Order>();

    // useEffect(() => {
    //     const fetchCart = async () => {
    //         const baskets = await fetchCartProduct();

    //         setPanier(baskets);
    //     };
    //     fetchCart();
    // }, [])

    useEffect(() => {
        fetchCartProduct().then(data => {
            setPanier(data);
        });

    }, []);

    return (
        <>
            <ItemCart order={panier} />
        </>
    )
}
