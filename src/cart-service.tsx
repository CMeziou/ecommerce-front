import axios from "axios";
import { LineProduct, Product } from "./entities";

export async function addtoCart( product:Product) {
    const response = await axios.post<LineProduct>('/api/cart/'+ product.id);
    return response.data;
}

export async function deleteFromCart(lineProduct:LineProduct) {
    await axios.delete('/api/cart/'+lineProduct.id);
}
