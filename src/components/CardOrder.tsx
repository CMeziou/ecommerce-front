import Sidebar from "@/components/SideBar";
import { Order, User } from "@/entities";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { fetchUser, fetchUserorder } from "@/auth/auth-service";
import Orders from "@/pages/account/orders";
import CardOrderDetail from "./CardOrderDetail";

interface Props {
  order: Order;
}

export default function CardOrder({order}:Props) {
  const [show, setShow] = useState(false);

  function toggle() {
    setShow(!show);
  }

  return (
    <>
      <div className="d-flex card m-3" style={{ width: 18 + "rem" }}>
        <div className="card-body bg-light">
          <>
            <p className="mb-0">{order.title} </p>
            <p key={order.id}></p>
            <p className="mb-0">
              {order.status}
            </p>
          </>
        </div>

        <div className="m-1">
          <button
            className="btn greyBackground btn-outline-dark"
            onClick={toggle}
          >
            voir plus
          </button>
        </div>

        <div className="container-fluid">
          <div className="row d-flex justify-content-center">
           
                {show && (
                        <CardOrderDetail key={order.id} order={order} />
                      )}
             
          </div>
        </div>
      </div>
    </>
  );
}
