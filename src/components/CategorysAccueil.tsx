import { Category } from "@/entities";
import { useRouter } from "next/router";
import React from "react";
import Link from "next/link";

interface Props {
  categorys: Category[];
}

export default function CategorysAccueil({ categorys }: Props) {
  const router = useRouter();

 
    return(
        
        <div>
       
        {categorys.map(cat =>
       
            <div key={cat.id} className="text-center ">
            <h2 className="text-center mt-5 mb-5">
            {cat.name}</h2>
            
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4 justify-content-center">
            { cat.products.slice(0,5).map(product =>
                <div className="card border border-0" style={{width: 18+"rem"}}>
                    <Link className='my-link' href={"/product/"+product.id}>
                {product.images?.length ?
                    <img className="card-img-top" key={product.id} src={product.images[0].link} alt="img-product" /> : <img src="https://kitedanmarkm1.b-cdn.net/media/catalog/product/cache/10/image/500x500/e4d92e6aceaad517e7b5c12e0dc06587/s/k/skateboard_skateboard_nkx_signature_8inch_blue-pink_00.jpg" alt="img-product" />
                }
                
                <div className="card-body">
                <p className="card-text d-flex justify-content-center">{product.title}</p>
                <p className="card-text d-flex justify-content-center">{product.price} €</p>
                </div>
                </Link>
              </div>
            )}
          </div>
          <button
            type="button"
            className="btn btn-dark mb-5 mt-4"
            onClick={() => router.push("/category/" + cat.id)}
          >
            Voir Plus
          </button>
        </div>
      )}
    </div>
  );
}
