import { Product } from "@/entities";
import { Button } from "@mui/material";
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import Link from "next/link";
import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";

interface Props {
    product: Product;
    onAction:() => void
}

export default function ItemProduct({ product, onAction }: Props) {

    const {token} = useContext(AuthContext);

    return (
        <>
        <div className="container-fluid">
        <div className="row d-flex justify-content-around">
        <div className="col-4">
        
        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
        <div className="carousel-item active">
        {product.images?.length ?
            <img className="card-img-top" key={product.id} src={product.images[0].link} alt="img-product" /> : <img src="https://kitedanmarkm1.b-cdn.net/media/catalog/product/cache/10/image/500x500/e4d92e6aceaad517e7b5c12e0dc06587/s/k/skateboard_skateboard_nkx_signature_8inch_blue-pink_00.jpg" alt="img-product" />
        }
        </div>
        {product.images?.slice(1,5).map(image =>
            <div key={product.id} className="carousel-item">
            <img src={image.link} className="d-block w-100" alt="..."/>
            </div> )}
            
            
            
            
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
            </button>
            </div>
            
            
            </div>
            
            <div className="col-7 card m-3" style={{width: 30+"rem"}}>
            <div className="card-body">
            <h3 className="card-title text-center mt-5"> {product?.title}</h3>
            <p className="mt-5">Description: {product.description}</p>
            <p className="card-text mt-5">Prix: {product?.price} €</p>
            {token ?
            <Button color="info" variant="contained" type="button" className="btn btn-primary mt-4 mb-4" onClick={onAction} endIcon={<AddShoppingCartIcon />}>Sélectionner</Button> : <div>
            <Button color="info" variant="contained" type="button" className="btn btn-primary mt-4 mb-4" disabled endIcon={<AddShoppingCartIcon />}>Sélectionner</Button>
            <p><small>Pour pouvoir ajouter ce produit à votre panier, vous devez vous <Link href="/login">connecter</Link>.</small></p></div>}
           
        </div>
        </div>
        
        
        </div>
        </div>
        </>
        )
    }
