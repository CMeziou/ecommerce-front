import axios from "axios";
import { Comment, Product } from "./entities";

export async function postComment(comment:Comment, product:Product) {
    const response = await axios.post<Comment>('/api/comment/'+ product.id, comment);
    return response.data;
}


