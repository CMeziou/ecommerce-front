import axios from "axios";
import { Category, Product } from "./entities";

export async function fetchAllCategory() {
  const response = await axios.get<Category[]>("/api/category");
  return response.data;
}

export async function fetchOneCategory(id: number | string) {
  const response = await axios.get<Category>("/api/category/" + id);
  return response.data;
}

export async function fetchCategoryProducts(id: number | string) {
  const response = await axios.get<Product[]>(`/api/category/${id}/products`);
  return response.data;
}
